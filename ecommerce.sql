create database ecommerce;

use ecommerce;

create table supplier(
    SUPP_ID  int primary key,
    SUPP_NAME varchar(50),
    SUPP_CITY varchar(50),
    SUPP_PHONE varchar(10) 
);

create table customer (
    CUS_ID int primary key,
    CUS_NAME varchar(50),
    CUS_PHONE varchar(10),
    CUS_CITY varchar(25),
    CUS_GENDER varchar(1)
);

create table category (
    CAT_ID int primary key,
    CAT_NAME varchar(10)
);

create table product (
    PRO_ID int primary key,
    PRO_NAME varchar(25),
    PRO_DESC varchar(25),
    CAT_ID int,
    foreign key(CAT_ID) references category(CAT_ID)
);

create table productDetails (
    PROD_ID int primary key,
    PRO_ID int,
    SUPP_ID int,
    PRICE float,
    foreign key(PRO_ID) references product(PRO_ID),
    foreign key(SUPP_ID) references supplier(SUPP_ID)
);

create table orders (
    ORD_ID int primary key,
    ORD_AMOUNT float,
    ORD_DATE date,
    CUS_ID int,
    PROD_ID int,
    foreign key(PROD_ID) references productDetails(PROD_ID),
    foreign key(CUS_ID) references customer(CUS_ID)
);

create table ratings(
    RAT_ID int primary key,
    CUS_ID int,
    SUPP_ID int,
    RAT_RATSTARS int,
    foreign key(CUS_ID) references customer(CUS_ID),
    foreign key(SUPP_ID) references supplier(SUPP_ID)
);

insert into `supplier` values (1,"Rajesh Retails","Delhi","1234567890");
insert into `supplier` values (2,"Appario Ltd.","Mumbai","2589631470");
insert into `supplier` values (3,"Knome products","Banglore","9785462315");
insert into `supplier` values (4,"Bansal Retails","Kochi","8975463285");
insert into `supplier` values (5,"Mittal Ltd.","Lucknow","7898456532");

insert into `customer` values(1,"AAKASH","9999999999","DELHI","M");
insert into `customer` values(2,"AMAN","9785463215","NOIDA","M");
insert into `customer` values(3,"NEHA","9999999999","MUMBAI","F");
insert into `customer` values(4,"MEGHA","9994562399","KOLKATA","F");
insert into `customer` values(5,"PULKIT","7895999999","LUCKNOW","M");

insert into `category` values(1,"BOOKS");
insert into `category` values(2,"GAMES");
insert into `category` values(3,"GROCERIES");
insert into `category` values(4,"ELECTRONICS");
insert into `category` values(5,"CLOTHES");

insert into `product` values(1,"GTA V","DFJDJFDJFDJFDJFJF",2);
insert into `product` values(2,"TSHIRT","DFDFJDFJDKFD",5);
insert into `product` values(3,"ROG LAPTOP","DFNTTNTNTERND",4);
insert into `product` values(4,"OATS","REURENTBTOTH",3);
insert into `product` values(5,"HARRY POTTER","NBEMCTHTJTH",1);

insert into `productDetails` values(1,1,2,1500);
insert into `productDetails` values(2,3,5,30000);
insert into `productDetails` values(3,5,1,3000);
insert into `productDetails` values(4,2,3,2500);
insert into `productDetails` values(5,4,1,1000);

insert into `orders` values(20,1500,"2021-10-12",3,5);
insert into `orders` values(25,30500,"2021-09-16",5,2);
insert into `orders` values(26,2000,"2021-10-05",1,1);
insert into `orders` values(30,3500,"2021-08-16",4,3);
insert into `orders` values(50,2000,"2021-10-06",2,1);

insert into `ratings` values(1,2,2,4);
insert into `ratings` values(2,3,4,3);
insert into `ratings` values(3,5,1,5);
insert into `ratings` values(4,1,3,2);
insert into `ratings` values(5,4,5,4);

select c.CUS_GENDER, count(*) as count from customer c inner join orders o on c.CUS_ID = o.CUS_ID where o.ORD_AMOUNT >= 3000 group by (c.CUS_GENDER);

select o.*,p.PRO_NAME from orders o,product p,productDetails pd where o.CUS_ID =2 and pd.PRO_ID = p.PRO_ID and pd.PROD_ID = o.PROD_ID;

select s.* from supplier s, productDetails pd where s.SUPP_ID in(select pd.SUPP_ID from productDetails pd group by pd.SUPP_ID having count(pd.PROD_ID) >1 ) group by s.SUPP_ID;

select c.* from category c, product p, productDetails pd, orders o where c.CAT_ID = p.CAT_ID and pd.PROD_ID = o.PROD_ID having min(o.ORD_AMOUNT);

select p.PROD_ID,p.PRO_NAME from product p, productDetails pd, orders o where  pd.PROD_ID = o.PROD_ID and pd.PRO_ID = p.PRO_ID and o.ORD_DATE> "2021-10-5";

select CUS_NAME,CUS_GENDER from customer where CUS_NAME like "A%" or CUS_NAME like "%A";

delimiter !
create procedure myProc()

begin
    select RAT_RATSTARS,
    case 
    when RAT_RATSTARS > 4 then 'Genuine Supplier'
    when RAT_RATSTARS < 2 then 'Average Supplier'
    else
    'Supplier should not be considered'
    End as verdict from ratings inner join supplier on supplier.SUPP_ID = ratings.SUPP_ID
end !

delimiter;